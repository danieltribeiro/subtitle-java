package danieltribeiro.subtitle;

import java.util.regex.*;

import danieltribeiro.subtitle.Subtitle;

import danieltribeiro.subtitle.SubtitleEntry;

public class SubFormat implements SubtitleFormat{

  String headerPattern = "(?<=\\*PART 1\\*\r?\n\\d\\d\\:\\d\\d\\:\\d\\d[\\.\\;\\:]\\d\\d\\\\\\d\\d\\:\\d\\d\\:\\d\\d[\\.\\;\\:]\\d\\d)";

  String footerPattern = "(?=\\*END\\*\r?\n)";

  String entryPattern = "(?<=\\d\\d\\:\\d\\d\\:\\d\\d[\\.\\;\\:]\\d\\d\\\\\\d\\d\\:\\d\\d\\:\\d\\d[\\.\\;\\:]\\d\\d)";

  // Pattern entryPartsPattern = Pattern.compile("(?<content>(.+\\r?\\n?))+(?<start>\\d\\d\\:\\d\\d\\:\\d\\d[\\.\\;\\:]\\d\\d)\\\\(?<end>\\d\\d\\:\\d\\d\\:\\d\\d[\\.\\;\\:]\\d\\d)");

  Pattern entryPartsPattern = Pattern.compile("(?<content>(.+\r?\n?)+)\r\n(?<start>\\d\\d\\:\\d\\d\\:\\d\\d[\\.\\;\\:]\\d\\d)\\\\(?<end>\\d\\d\\:\\d\\d\\:\\d\\d[\\.\\;\\:]\\d\\d)");

  @Override
  public Subtitle parse(String text) {

    Subtitle subtitle = new Subtitle();
    subtitle.setOriginalFormat("SUB");

    String[] arr = text.split(headerPattern.toString());
    if (arr.length > 1) {
      subtitle.setHeader(arr[0]);
      arr = new String[]{arr[1]};
    }
    
    arr = arr[0].split(footerPattern.toString());
    if (arr.length > 1) {
      subtitle.setFooter(arr[1]);
      arr = new String[]{arr[0]};
    }

    arr = arr[0].split(entryPattern.toString());

    for (String entryAsText : arr) {
      String[] entryArr = entryAsText.split("(?=\\d\\d\\:\\d\\d\\:\\d\\d[\\.\\;\\:]\\d\\d\\\\\\d\\d\\:\\d\\d\\:\\d\\d[\\.\\;\\:]\\d\\d)");
      if (entryArr.length > 1) {
        SubtitleEntry e = new SubtitleEntry();
        if (entryArr[0].startsWith("\r\n")) {
          entryArr[0] = entryArr[0].substring(2);
        }
        if (entryArr[0].endsWith("\r\n")) {
          entryArr[0] = entryArr[0].substring(0, entryArr[0].length() - 2);
        }
        // e.setContent(entryArr[0]);
        e.setVisibleContent(!entryArr[0].startsWith("*"));

        e.setLines(entryArr[0].split("\r?\n"));

        entryArr = entryArr[1].split("\\\\");
        e.setStart(entryArr[0]);
        e.setEnd(entryArr[1]);
        subtitle.getEntries().add(e);
      }
    }
    return subtitle;
  }

  @Override
  public String format(Subtitle subtitle) {
    StringBuffer bf = new StringBuffer();
    boolean firstLine = true;
    if (subtitle.getHeader() != null) {
      bf.append(subtitle.getHeader());
      bf.append("\r\n");
    }


    if (subtitle.getEntries() != null) {
        for (SubtitleEntry e : subtitle.getEntries()) {
            if (!firstLine) {
                bf.append("\r\n");
            }
            if (e.getLines() != null) {
                bf.append(String.join("\r\n", e.getLines()));
                bf.append("\r\n");
                bf.append(e.getStart());
                bf.append("\\");
                bf.append(e.getEnd());
            }
            firstLine = false;
        }
        bf.append("\r\n");
    }

    if (subtitle.getFooter() != null) {
      bf.append(subtitle.getFooter());
    }
    return bf.toString();
  }
}