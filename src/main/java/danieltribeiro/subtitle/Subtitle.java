package danieltribeiro.subtitle;

import java.util.ArrayList;
import java.util.List;

public class Subtitle {

  private String originalFormat;

  private String name;

  private String frameRate;

  private String header;

  private String footer;

  private List<SubtitleEntry> entries = new ArrayList<>();

  /**
   * @return the entries
   */
  public List<SubtitleEntry> getEntries() {
    return entries;
  }

  /**
   * @param entries the entries to set
   * @return This Subtitle
   */
  public Subtitle setEntries(List<SubtitleEntry> entries) {
    this.entries = entries;
    return this;
  }

  /**
   * @return the footer
   */
  public String getFooter() {
    return footer;
  }

  /**
   * @param footer the footer to set
   * @return This Subtitle
   */
  public Subtitle setFooter(String footer) {
    this.footer = footer;
    return this;
  }

  /**
   * @return the header
   */
  public String getHeader() {
    return header;
  }

  /**
   * @param header the header to set
   * @return This Subtitle
   */
  public Subtitle setHeader(String header) {
    this.header = header;
    return this;
  }

  /**
   * @return the name
   */
  public String getName() {
    return name;
  }

  /**
   * @param name the name to set
   * @return This SubtitleEntry
   */
  public Subtitle setName(String name) {
    this.name = name;
    return this;
  }

  /**
   * @return the frameRate
   */
  public String getFrameRate() {
    return frameRate;
  }

  /**
   * @param frameRate the frameRate to set
   * @return This SubtitleEntry
   */
  public Subtitle setFrameRate(String frameRate) {
    this.frameRate = frameRate;
    return this;
  }

  /**
   * 
   * @return The first Entry of the subtitle or null if entries is empty
   */
  public SubtitleEntry firstEntry() {
      if (entries == null) {
          return null;
      }
      if (entries.size() < 1) {
        return null;
    }
    return entries.get(0);
  }

  /**
   * @return The last Entry of the subtitle or null if entries is empty
   */
  public SubtitleEntry lastEntry() {
      if (entries == null) {
          return null;
      }
      if (entries.size() < 1) {
        return null;
    }
    return entries.get(entries.size() - 1);
  }

  /**
   * 
   * @return The index of the first Visible Entry or null
   */
  public int firstVisibleIndex() {
    if (entries != null) {
        for (int i = 0; i < entries.size(); i++) {
            if (entries.get(i).isVisibleContent()) {
                return i;
            }
        }
    }
    return -1;
  }

  /**
   * 
   * @return The first Visible Entry
   */
  public SubtitleEntry firstVisibleEntry() {
    int index = firstVisibleIndex();
    if (index == -1) {
      return null;
    } else {
      return entries.get(index);
    }
  }

  /**
   * @param originalFormat the originalFormat to set
   */
  public void setOriginalFormat(String originalFormat) {
    this.originalFormat = originalFormat;
  }

  /**
   * @return the originalFormat
   */
  public String getOriginalFormat() {
    return originalFormat;
  }

  
}
