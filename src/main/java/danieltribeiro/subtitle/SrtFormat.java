package danieltribeiro.subtitle;

import java.util.ArrayList;
import java.util.List;

import danieltribeiro.subtitle.SubtitleEntry;

public class SrtFormat implements SubtitleFormat{

  String entryPattern = "(?=\\d\\n\\d\\d\\:\\d\\d\\:\\d\\d\\,\\d\\d\\d \\-\\-\\> \\d\\d\\:\\d\\d\\:\\d\\d\\,\\d\\d\\d)";

  @Override
  public Subtitle parse(String text) {

    Subtitle subtitle = new Subtitle();
    subtitle.setOriginalFormat("SRT");

    String[] arr = text.replaceAll("\r", "").split("\n\n");

    for (String entryAsText : arr) {
      String[] entryArr = entryAsText.split("\r?\n");
      if (entryArr.length > 1) {
        SubtitleEntry e = new SubtitleEntry();

        String[] times = entryArr[1].split(" \\-\\-\\> ");
        e.setStart(times[0].trim());
        e.setEnd(times[1].trim());

        List<String> lines = new ArrayList<>();
        if (entryArr.length > 2) {
            if (entryArr[2].trim().length() > 0) {
                lines.add(entryArr[2]);
            }
        }
        if (entryArr.length > 3) {
            if (entryArr[3].trim().length() > 0) {
                lines.add(entryArr[3]);
            }
        }
        
        e.setContent(String.join("\n", lines));
        e.setVisibleContent(!e.getContent().startsWith("*"));

        e.setLines(lines.toArray(new String[0]));

        subtitle.getEntries().add(e);
      }
    }
    return subtitle;
  }

  @Override
  public String format(Subtitle subtitle) {
    StringBuffer bf = new StringBuffer();
    if (subtitle.getHeader() != null) {
      bf.append(subtitle.getHeader());
      bf.append("\n");
    }
    int idx = 1;
    for (SubtitleEntry e : subtitle.getEntries()) {
      bf.append("" + idx++ + "\n");
      bf.append(e.getStart());
      bf.append(" --> ");
      bf.append(e.getEnd());
      bf.append("\n");
      bf.append(String.join("\n", e.getLines()));
      bf.append("\n");
      bf.append("\n");
    }
    if (subtitle.getFooter() != null) {
        bf.append("\r\n");
        bf.append(subtitle.getFooter());
    }
    return bf.toString();
  }
}