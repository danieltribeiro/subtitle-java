package danieltribeiro.subtitle;

import danieltribeiro.subtitle.Subtitle;

public interface SubtitleFormat {

  public Subtitle parse(String text);

  public String format(Subtitle subtitle);

}