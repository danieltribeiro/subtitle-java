package danieltribeiro.subtitle;

public class SubtitleEntry {
    
    private String start;

    private String end;

    private String params;

    private String content;

    private String lines[];

    private boolean visibleContent;


    /**
     * @param lines the lines to set
     * @return this SubtitleEntry
     */
    public SubtitleEntry setLines(String[] lines) {
      this.lines = lines;
      return this;
    }
    /**
     * @return the lines
     */
    public String[] getLines() {
      return lines;
    }
    
    /**
     * @param visibleContent the visibleContent to set
     * @return This SubtitleEntry
     */
    public SubtitleEntry setVisibleContent(boolean visibleContent) {
      this.visibleContent = visibleContent;
      return this;
    }

    /**
     * @return the visibleContent
     */
    public boolean isVisibleContent() {
      return visibleContent;
    }
    

    /**
     * @return the content
     */
    public String getContent() {
      return content;
    }

    /**
     * 
     * @param content The content of this entry
     * @return this SubtitleEntry
     */
    public SubtitleEntry setContent(String content) {
      this.content = content;
      return this;
    }

    /**
     * @return the end
     */
    public String getEnd() {
      return end;
    }

    /**
     * 
     * @param end The end of this entry
     * @return this SubtitleEntry
     */
    public SubtitleEntry setEnd(String end) {
      this.end = end;
      return this;
    }

    /**
     * @return the params
     */
    public String getParams() {
      return params;
    }

    /**
     * 
     * @param params The params of this entry
     * @return this SubtitleEntry
     */
    public SubtitleEntry setParams(String params) {
      this.params = params;
      return this;
    }

    /**
     * @return the start
     */
    public String getStart() {
      return start;
    }

    /**
     * 
     * @param start The start of this entry
     * @return this SubtitleEntry
     */
    public SubtitleEntry setStart(String start) {
      this.start = start;
      return this;
    }
  }
