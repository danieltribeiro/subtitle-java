package danieltribeiro.subtitle;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import danieltribeiro.subtitle.Subtitle;

public class SrtFormatTests {

  private SrtFormat srtFormat = new SrtFormat();

  private String sampleText = "1\n"
  + "01:00:03,646 --> 01:00:05,147\n"
  + "<i>Anteriormente,</i>\n"
  + "<i>em </i>The Good Doctor...\n"
  + "\n"
  + "2\n"
  + "01:00:05,231 --> 01:00:06,816\n"
  + "Shaun, eu tenho câncer.\n"
  + "\n"
  + "3\n"
  + "01:00:06,899 --> 01:00:09,443\n"
  + "Vão operar meu cérebro,\n"
  + "é assustador.\n"
  + "\n";

  private String sampleText2 = "1\n"
  + "00:00:01,600 --> 00:00:04,200\n"
  + "English (US)\n"
  + "\n"
  + "2\n"
  + "00:00:05,900 --> 00:00:07,999\n"
  + "This is a subtitle in American English\n"
  + "\n"
  + "3\n"
  + "00:00:10,000 --> 00:00:14,000\n"
  + "Adding subtitles is very easy to do";

    @Test public void testParse() {
      Subtitle subtitle = srtFormat.parse(sampleText);
      assertEquals("01:00:03,646", subtitle.getEntries().get(0).getStart());
      assertEquals("Vão operar meu cérebro,", subtitle.getEntries().get(2).getLines()[0]);
      assertEquals("<i>em </i>The Good Doctor...", subtitle.getEntries().get(0).getLines()[1]);
    }

    @Test public void testParse2() {
      Subtitle subtitle = srtFormat.parse(sampleText2);
      assertEquals("00:00:01,600", subtitle.getEntries().get(0).getStart());
      assertEquals(3, subtitle.getEntries().size());
      
    }

    
    @Test public void testFormat() {
        System.out.println(sampleText);
        Subtitle subtitle = srtFormat.parse(sampleText);
        System.out.println(srtFormat.format(subtitle));
        assertEquals(sampleText, srtFormat.format(subtitle));
    }
}