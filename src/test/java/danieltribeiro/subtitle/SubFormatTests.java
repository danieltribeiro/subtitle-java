package danieltribeiro.subtitle;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import danieltribeiro.subtitle.Subtitle;

public class SubFormatTests {

  private SubFormat subFormat = new SubFormat();

  private String header = "*PART 1*\r\n"
    + "00:00:00.00\\00:00:00.00";

  private String footer = "*END*\r\n"
    + "  ............\\\\............\r\n"
    + "*CODE*\r\n"
    + "00000000000000000";

  private String sampleText = "Primeira Linha\r\n"
    + "01:02:03.04\\01:02:09.04\r\n"
    + "Segunda Linha\r\n"
    + "  Aqui tem 2 linhas\r\n"
    + "01:02:13.04\\01:02:19.04\r\n"
    + "  Terceira Linha\r\n"
    + "01:02:23.04\\01:02:29.04";

    @Test public void testParse() {
      Subtitle subtitle = subFormat.parse(header + "\r\n" + sampleText + "\r\n" + footer);
      assertEquals("Header invalido!", header, subtitle.getHeader());
      assertEquals("Footer invalido!", footer.trim(), subtitle.getFooter().trim());
      assertEquals("01:02:03.04", subtitle.getEntries().get(0).getStart());
      assertEquals("Segunda Linha", subtitle.getEntries().get(1).getLines()[0]);
      assertEquals("  Aqui tem 2 linhas", subtitle.getEntries().get(1).getLines()[1]);
    }
  
    @Test public void testFormat() {
      String txt = header + "\r\n" + sampleText + "\r\n" + footer;
      Subtitle subtitle = subFormat.parse(txt);
      assertEquals(txt, subFormat.format(subtitle));
    }
}